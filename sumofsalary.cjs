function sumofsalary(obj) {

   let salarySum = obj.reduce((total, { salary }) => {
      return total + parseFloat(salary.slice(1))
   }, 0)
   return salarySum
}

module.exports = sumofsalary