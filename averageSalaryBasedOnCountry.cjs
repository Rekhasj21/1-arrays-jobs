function averageSalaryBasedOnCountry(obj) {
    const result = obj.reduce((acc, person) => {
        const { location, salary } = person;
        const salaryInt = parseFloat(salary.slice(1));

        if (!acc[location]) {
            acc[location] = { totalSalary: salaryInt, count: 1 };
        } else {
            acc[location].totalSalary += salaryInt;
            acc[location].count++;
        }

        return acc;
    }, {});

    const averageSalary = {};
    for (let key in result) {
        averageSalary[key] = result[key].totalSalary / result[key].count;
    }

    return averageSalary;
}

module.exports = averageSalaryBasedOnCountry