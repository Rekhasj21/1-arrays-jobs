function newSalary(obj) {
    const newObj = obj.map(person => {
        return { ...person, new_salary: parseFloat(person.salary.slice(1)) * 10000 }
    })
    return newObj
}

module.exports = newSalary