function sumofsalarybasedoncountry(obj) {
    
    const result = obj.reduce((acc, person) => {
        const { location, salary } = person;
        const salaryInt = parseFloat(salary.slice(1));
        if (!acc[location]) {
            acc[location] = salaryInt;
        } else {
            acc[location] += salaryInt;
        }
        return acc;
    }, {});
    return result
}

module.exports = sumofsalarybasedoncountry