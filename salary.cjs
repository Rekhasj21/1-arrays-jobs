function salary(obj) {

    const object = obj.map(person => {

        return { ...person, salary: parseFloat(person.salary.slice(1)) }

    })
    return object
}

module.exports = salary